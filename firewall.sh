#!/bin/bash
#
# configure firewall

apt install ufw
ufw default allow outgoing
ufw default deny incoming
ufw allow ssh
ufw enable
