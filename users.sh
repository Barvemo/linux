#!/bin/bash
#
# add users to system

USERS=('linus')

for user in "${USERS[@]}"; do
  adduser "$user" --disabled-password --gecos GECOS
  adduser "$user" sudo
  mkdir /home/"$user"/.ssh
  chmod 700 /home/"$user"/.ssh
  chown "$user":"$user" /home/"$user"/.ssh
  cp ~/linux/ssh/id_ed25519.pub /home/"$user"/.ssh/
  cp ~/linux/ssh/authorized_keys /home/"$user"/.ssh/
  chown "$user":"$user" /home/"$user"/.ssh/*
  chmod 600 /home/"$user"/.ssh/*
done
