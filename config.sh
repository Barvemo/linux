#!/bin/bash
#
# configure system

PACKAGES=('tmux' 'zsh')

for package in "${PACKAGES[@]}"; do
  apt -y install "$package"
done
