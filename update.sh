#!/bin/bash
#
# Update and upgrade the system

# update repositories
apt -y update

# upgrade system
apt -y upgrade

if [ -f /var/run/reboot-required ]; then
	    echo -e $TEXT_RED_B
	        echo 'Reboot required!'
		    echo -e $TEXT_RESET
		    	reboot
fi
